/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UtilsARP;

import Negocio.Subred;
import Negocio.Test;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import jpcap.JpcapCaptor;
import jpcap.JpcapSender;
import jpcap.NetworkInterface;
import jpcap.NetworkInterfaceAddress;
import jpcap.packet.ARPPacket;
import jpcap.packet.EthernetPacket;
import jpcap.packet.IPPacket;
import jpcap.packet.TCPPacket;

/**
 *
 * @author SANTI
 */
public class Utils {

    public static void RecibirPaquetes() {
        JpcapCaptor captor;
        try {
            NetworkInterface[] devices = JpcapCaptor.getDeviceList();
            captor = JpcapCaptor.openDevice(devices[2], 65535, false, 1000);

            captor.loopPacket(-1, new PacketPrinter());
            captor.close();
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void SleeccionarInterfaz(int index , int timeout) {
        NetworkInterface[] devices = JpcapCaptor.getDeviceList();
        try {
            JpcapCaptor captor = JpcapCaptor.openDevice(devices[index], 65535, false, timeout);
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static List<Subred> ObetenerDispositivosInterfaz() {

        NetworkInterface[] devices = JpcapCaptor.getDeviceList();
        List<Subred> lista = new ArrayList<Subred>();
        String ip = "";
        String MAC = "";
        String Nombre = "";
        String Mascara = "";
        String puertanelace = "";

        for (int i = 0; i < devices.length; i++) {

            int k = 0;
            Mascara = "";
            MAC = "";
            Nombre = devices[i].description;
            for (byte b : devices[i].mac_address) {
                MAC += Integer.toHexString(b & 0xff) + ":";
            }

            for (NetworkInterfaceAddress a : devices[i].addresses) {

                if (k > 0) {
                    if (a.subnet != null && a.address != null) {
                        Mascara = a.subnet.toString();
                        ip = a.address.toString();
                        puertanelace = a.broadcast.toString();
                    }
                }
                k++;
            }

            lista.add(new Subred(ip.replace("/", ""), MAC, Mascara.replace("/", ""), Nombre, devices[i], puertanelace));
        }
        return lista;
    }

    public static NetworkInterface ObetenerDispositivosInterfazIndex( int i) {
       NetworkInterface[] devices = JpcapCaptor.getDeviceList();
       return devices[i] ;
    }

    public static byte[] enviarPaqueteARP(ARPPacket arp, NetworkInterface subred , int timeout) throws IOException {
        JpcapCaptor captor = JpcapCaptor.openDevice(subred, 2000, false, timeout);
        captor.setFilter("arp", true);
        JpcapSender sender = captor.getJpcapSenderInstance();
        sender.sendPacket(arp);
        while (true) {
            ARPPacket p = (ARPPacket) captor.getPacket();
            if (p == null) {
                captor.close();
                return null;
            } else if (Arrays.equals(p.target_protoaddr, obtenerToInetAddress(subred).getAddress())) {
                captor.close();
                return p.sender_hardaddr;
            }
        }
    }

    public static String protocoloARP(String ipDestino, NetworkInterface subred , int timeout) throws IOException {
        InetAddress inetAddressIp = InetAddress.getByName(ipDestino);
        byte[] broadcast = new byte[]{(byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255};
        ARPPacket arp = new ARPPacket();
        arp.hardtype = ARPPacket.HARDTYPE_ETHER;
        arp.prototype = ARPPacket.PROTOTYPE_IP;
        arp.operation = ARPPacket.ARP_REQUEST;
        arp.hlen = 6;
        arp.plen = 4;
        arp.sender_hardaddr = subred.mac_address;
        arp.sender_protoaddr = obtenerToInetAddress(subred).getAddress();
        arp.target_hardaddr = broadcast;
        arp.target_protoaddr = inetAddressIp.getAddress();

        EthernetPacket ether = new EthernetPacket();
        ether.frametype = EthernetPacket.ETHERTYPE_ARP;
        ether.src_mac = subred.mac_address;
        ether.dst_mac = broadcast;
        arp.datalink = ether;
        byte[] respuesta = enviarPaqueteARP(arp, subred , timeout);
        if (respuesta != null) {
            return byteToString(respuesta);
        }
        return null;
    }

    public static InetAddress obtenerToInetAddress(NetworkInterface subred) {
        InetAddress direccion = null;
        for (int i = 0; i < subred.addresses.length; i++) {
            if (subred.addresses[i].address instanceof Inet4Address) {
                direccion = subred.addresses[i].address;
                break;
            }
        }
        return direccion;
    }

    public static List<String> obtenerSubRedes(String ip, String mascara) {
        List<String> sR = new ArrayList<String>();
        String clase = "";
        StringTokenizer sepMasc = new StringTokenizer(mascara, ".");
        String campo1 = "", campo2 = "", campo3 = "";
        campo1 = sepMasc.nextToken();
        campo2 = sepMasc.nextToken();
        campo3 = sepMasc.nextToken();

        if (campo1.equals("255") && !campo2.equals("255") && !campo3.equals("255")) {
            clase = "A";
        }
        if (campo1.equals("255") && campo2.equals("255") && !campo3.equals("255")) {
            clase = "B";
        }
        if (campo1.equals("255") && campo2.equals("255") && campo3.equals("255")) {
            clase = "C";
        }
        StringTokenizer sepIp = new StringTokenizer(ip, ".");
        campo1 = sepIp.nextToken();
        campo2 = sepIp.nextToken();
        campo3 = sepIp.nextToken();

        if (clase.equals("C")) {
            for (int i = 0; i < 256; i++) {

                if (!ip.equals(campo1 + "." + campo2 + "." + campo3 + "." + Integer.toString(i))) {
                    sR.add(campo1 + "." + campo2 + "." + campo3 + "." + Integer.toString(i));
                }
            }
        }
        if (clase.equals("B")) {
            for (int i = 0; i < 256; i++) {
                for (int j = 0; j < 256; j++) {
                    if (!ip.equals(campo1 + "." + campo2 + "." + Integer.toString(i) + "." + Integer.toString(j))) {
                        sR.add(campo1 + "." + campo2 + "." + Integer.toString(i) + "." + Integer.toString(j));
                    }

                }
            }
        }
        if (clase.equals("A")) {
            for (int i = 0; i < 256; i++) {
                for (int j = 0; j < 256; j++) {
                    for (int k = 0; k < 256; k++) {
                        if (!ip.equals(campo1 + "." + Integer.toString(i) + "." + Integer.toString(j) + "." + Integer.toString(k))) {
                            sR.add(campo1 + "." + Integer.toString(i) + "." + Integer.toString(j) + "." + Integer.toString(k));
                        }

                    }
                }
            }
        }
        return sR;
    }

    public static String byteToString(byte[] direccion) {

        StringBuilder aux = new StringBuilder(18);
        for (byte b : direccion) {
            if (aux.length() > 0) {
                aux.append(':');
            }
            aux.append(String.format("%02x", b));
        }
        return aux.toString();
    }

}
