/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Negocio.Dispositivo;
import Negocio.EstadosEnum;
import Negocio.Sistema;
import Persistencia.ManejoDeArchivos;
import UtilsARP.Utils;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.NoSuchElementException;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import jpcap.NetworkInterface;

/**
 *
 * @author AndrésFelipe
 */
public class Principal extends javax.swing.JFrame {

    private GridBagLayout layout = new GridBagLayout();
    private Sistema sistema;
    private Timer timer = new Timer();
    private long time;
    private int numPendiente = 5;
    private long tiempoMax = 1000000;
    private long tiempocada = 5000;
    private int timeout = 500;

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public long getTiempocada() {
        return tiempocada;
    }

    public void setTiempocada(long tiempocada) {
        this.tiempocada = tiempocada;
    }

    public int getNumPendiente() {
        return numPendiente;
    }

    public void setNumPendiente(int numPendiente) {
        this.numPendiente = numPendiente;
    }

    public long getTiempoMax() {
        return tiempoMax;
    }

    public void setTiempoMax(long tiempoMax) {
        this.tiempoMax = tiempoMax;
    }

    TimerTask task = new TimerTask() {

        @Override
        public void run() {
            try {
                enviarTramaConsecutiva(timeout);
            } catch (IOException ex) {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Holi");
        }
    };

    /**
     * Creates new form Principal
     */
    private GridBagConstraints c = new GridBagConstraints();

    public Principal() {
        initComponents();

        this.setLocationRelativeTo(null);
        sistema = new Sistema(Utils.ObetenerDispositivosInterfaz());

        c.gridx = 0;
        c.gridy = 0;
        Panelprin.setLayout(layout);
        MenuP p = new MenuP(this);

        p.setVisible(true);
        Panelprin.removeAll();
        Panelprin.add(p, c);
        Panelprin.revalidate();
        Panelprin.repaint();

        Panelprin.setVisible(true);
    }

    public void PanelaSubred() {
        InfrodeRed p = new InfrodeRed(this);

        p.setVisible(true);
        Panelprin.removeAll();
        Panelprin.add(p, c);
        Panelprin.revalidate();
        Panelprin.repaint();

        Panelprin.setVisible(true);
    }

    public void PanelaDisp() {
        PPrincipal p = new PPrincipal(this, sistema);

        p.setVisible(true);
        Panelprin.removeAll();
        Panelprin.add(p, c);
        Panelprin.revalidate();
        Panelprin.repaint();

        Panelprin.setVisible(true);
    }

    public void PanelaEnviarTrama() {
        EnviarTrama p = new EnviarTrama(this);

        p.setVisible(true);
        Panelprin.removeAll();
        Panelprin.add(p, c);
        Panelprin.revalidate();
        Panelprin.repaint();

        Panelprin.setVisible(true);
    }

    public void editarDisp(String nombre, String tipo) {
        for (Dispositivo aux : sistema.getDispositivos()) {
            if (aux.getNombre() == nombre) {
                aux.setTipo(tipo);
            }
        }

    }

    public void PanelaRepresentarDisp() {
        RepresentarDisp p = new RepresentarDisp(this);

        p.setVisible(true);
        Panelprin.removeAll();
        Panelprin.add(p, c);
        Panelprin.revalidate();
        Panelprin.repaint();

        Panelprin.setVisible(true);
    }

    public void PanelaPrincipal() {
        MenuP p = new MenuP(this);

        p.setVisible(true);
        Panelprin.removeAll();
        Panelprin.add(p, c);
        Panelprin.revalidate();
        Panelprin.repaint();
        System.out.println(sistema.getSelectedint());
        Panelprin.setVisible(true);
    }

    public void tiempocada1() {
        task = new TimerTask() {

            @Override
            public void run() {
                try {
                    enviarTramaConsecutiva(timeout);
                } catch (IOException ex) {
                    Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("Holi");
            }
        };
        timer.schedule(task, 10, tiempocada);

    }

    public void paratimer() {
        task.cancel();
        timer.purge();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panelprin = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        Prueba = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Panelprin.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Panelprin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelprinMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout PanelprinLayout = new javax.swing.GroupLayout(Panelprin);
        Panelprin.setLayout(PanelprinLayout);
        PanelprinLayout.setHorizontalGroup(
            PanelprinLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 617, Short.MAX_VALUE)
        );
        PanelprinLayout.setVerticalGroup(
            PanelprinLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 397, Short.MAX_VALUE)
        );

        jMenu1.setText("Archivo");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Guardar.jpg"))); // NOI18N
        jMenuItem2.setText("Guardar Sistema");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/Cargar.png"))); // NOI18N
        jMenuItem1.setText("Cargar Sistema");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        Prueba.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        Prueba.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/salir.png"))); // NOI18N
        Prueba.setText("Salir");
        Prueba.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PruebaMouseClicked(evt);
            }
        });
        Prueba.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PruebaActionPerformed(evt);
            }
        });
        jMenu1.add(Prueba);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Opciones");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panelprin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panelprin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void PanelprinMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PanelprinMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_PanelprinMouseClicked

    private void PruebaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PruebaActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_PruebaActionPerformed

    private void PruebaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PruebaMouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_PruebaMouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser(".");
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String pathArchivo = chooser.getSelectedFile().getParent();
            String nombreArchivo = chooser.getSelectedFile().getName();
            try {
                sistema = ManejoDeArchivos.cargarSistema(nombreArchivo, pathArchivo);
                JOptionPane.showMessageDialog(this, "Sistema cargado correctamente");
            } catch (FileNotFoundException e1) {
                JOptionPane.showMessageDialog(this, e1.getMessage());
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(this, e1.getMessage());
            } catch (NoSuchElementException e1) {
                JOptionPane.showMessageDialog(this, e1.getMessage());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed
    public void enviarTramaConsecutiva( int timeout) throws IOException {
        String mac = null;
         
        for (Dispositivo aux : sistema.getDispositivos()) {
            System.out.println("IP " + aux.getIp());
            mac = Utils.protocoloARP(aux.getIp(), Utils.ObetenerDispositivosInterfazIndex(sistema.getSelectedint()) , timeout);
            System.out.println("IP " + aux.getIp() + " MAC" + mac);
            if (mac == null) {
                aux.setTramasNoConstestadas(aux.getTramasNoConstestadas() + 1);
                if (aux.getTiempoEspera() == 0) {
                    aux.setTiempoEspera(System.currentTimeMillis());
                }

                if (aux.getTramasNoConstestadas() <= numPendiente) {
                    aux.setEstado(EstadosEnum.PENDIENTE.toString());
                    break;
                }
                if (aux.getTramasNoConstestadas() > numPendiente) {
                    aux.setEstado("Eliminar");
                    break;

                }
                long timeEnd = System.currentTimeMillis();
                if (timeEnd - aux.getTiempoEspera() > tiempoMax) {
                    aux.setEstado("Eliminar");
                    break;
                }//corregir
                aux.setEstado(EstadosEnum.INACTIVO.toString());
                break;
            }
            if (mac != null) {
                aux.setTramasNoConstestadas(0);
                aux.setEstado(EstadosEnum.ACTIVO.toString());
            }

        }
        List<Integer> eliminaciones = new ArrayList<Integer>();
        int i = 0;
        for (Dispositivo aux : sistema.getDispositivos()) {
            if (aux.getEstado().equals("Eliminar")) {
                eliminaciones.add(i);
            }
            i++;
        }
        for (int j = 0; j < eliminaciones.size(); i++) {
            sistema.getDispositivos().remove(eliminaciones.get(j));
        }

    }
public void AsignarDispositivomanual(int timeout , List<String> w) throws IOException {
        List<String> sR = w ;
        System.out.println("W : a" + sR.get(0)  + "aaaaa");
        for (int i = 0; i < sR.size(); i++) {
            String mac = Utils.protocoloARP(sR.get(i), Utils.ObetenerDispositivosInterfazIndex(sistema.getSelectedint()), timeout);
            if (mac != null) {
                Dispositivo disp = new Dispositivo(sR.get(i), mac);
                System.out.println("IP : " + sR.get(i) + " MAC " + mac);
                sistema.getDispositivos().add(disp);
            }
        }
    }
    public void AsignarDispositivo(int timeout) throws IOException {
        List<String> sR = Utils.obtenerSubRedes(sistema.getSubredes().get(sistema.getSelectedint()).getIp(), sistema.getSubredes().get(sistema.getSelectedint()).getMascara());
        for (int i = 0; i < sR.size(); i++) {
             System.out.println("IP : " + sR.get(i) );
            String mac = Utils.protocoloARP(sR.get(i), Utils.ObetenerDispositivosInterfazIndex(sistema.getSelectedint()) , timeout);
            if (mac != null) {
                Dispositivo disp = new Dispositivo(sR.get(i), mac);
                System.out.println("IP : " + sR.get(i) + " MAC " + mac);
                sistema.getDispositivos().add(disp);
            }
        }
    }
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser("./data");
        int returnVal = chooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String pathArchivo = chooser.getSelectedFile().getParent();
            String nombreArchivo = chooser.getSelectedFile().getName();
            try {
                ManejoDeArchivos.guardarSistema(sistema, pathArchivo, nombreArchivo);
                JOptionPane.showMessageDialog(this, "Sistema guardado corréctamente");
            } catch (FileNotFoundException e1) {
                JOptionPane.showMessageDialog(this, e1.getMessage());
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(this, e1.getMessage());
            } catch (NoSuchElementException e1) {
                JOptionPane.showMessageDialog(this, e1.getMessage());
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(this, "problema al guardar archivo: " + e1.getMessage());
            }
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed
    public Sistema getSistema() {
        return sistema;
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panelprin;
    private javax.swing.JMenuItem Prueba;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    // End of variables declaration//GEN-END:variables
}
