/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.io.Serializable;
import jpcap.NetworkInterface;

/**
 *
 * @author SANTI
 */
public class Subred implements Serializable {

    private String ip;
    private String MAC;
    private String mascara;
    private String nombre;
    private String puertaenlace ;

    public String getPuertaenlace() {
        return puertaenlace;
    }

    public void setPuertaenlace(String puertaenlace) {
        this.puertaenlace = puertaenlace;
    }
    

   

    public Subred(String ip, String MAC, String mascara, String nombre , NetworkInterface interface2 , String puertaenlace) {
        this.ip = ip;
        this.MAC = MAC;
        this.mascara = mascara;
        this.nombre = nombre;

        this.puertaenlace = puertaenlace ;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public String getMascara() {
        return mascara;
    }

    public void setMascara(String mascara) {
        this.mascara = mascara;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
