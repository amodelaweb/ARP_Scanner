/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.io.Serializable;

/**
 *
 * @author SANTI
 */
public class Dispositivo implements Serializable{

    private String ip;
    private String mac;
    private String tipo;
    private String estado;
    private String nombre;
    private int tramasNoConstestadas;
    private long tiempoEspera;
    private static int cons = 0;    

    public int getTramasNoConstestadas() {
        return tramasNoConstestadas;
    }

    public void setTramasNoConstestadas(int tramasNoConstestadas) {
        this.tramasNoConstestadas = tramasNoConstestadas;
    }

    public long getTiempoEspera() {
        return tiempoEspera;
    }

    public void setTiempoEspera(long tiempoEspera) {
        this.tiempoEspera = tiempoEspera;
    }

    public static int getCons() {
        return cons;
    }

    public static void setCons(int cons) {
        Dispositivo.cons = cons;
    }
    
    public Dispositivo(String ip, String mac) {
        this.ip = ip;
        this.mac = mac;
        this.nombre = "Disp_" + cons;
        this.tipo = TiposEnum.OTRO.toString() ;
        this.estado = EstadosEnum.ACTIVO.toString() ;
        cons++;

    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



}
