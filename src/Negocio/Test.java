package Negocio;

import UtilsARP.Utils;
import UtilsARP.PacketPrinter;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import jpcap.*;
import jpcap.packet.*;

import java.io.IOException;
import java.lang.Runtime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Test {

    public static void main(String args[]) throws IOException {
        /*
        try {
            System.out.println(Utils.protocoloARP("169.254.218.0",Utils.ObetenerDispositivosInterfazIndex(Utils.ObetenerDispositivosInterfaz() , 2 ) ) );
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
         */
        obtenerSubRedes("192.168.0.11", "255.255.255.0");
    }

    public static void obtenerSubRedes(String ip, String mascara) {
        List<String> sR = new ArrayList<String>();
        String clase = "";
        StringTokenizer sepMasc = new StringTokenizer(mascara, ".");
        String campo1 = "", campo2 = "", campo3 = "";
        campo1 = sepMasc.nextToken();
        campo2 = sepMasc.nextToken();
        campo3 = sepMasc.nextToken();

        if (campo1.equals("255") && !campo2.equals("255") && !campo3.equals("255")) {
            clase = "A";
        }
        if (campo1.equals("255") && campo2.equals("255") && !campo3.equals("255")) {
            clase = "B";
        }
        if (campo1.equals("255") && campo2.equals("255") && campo3.equals("255")) {
            clase = "C";
        }
        StringTokenizer sepIp = new StringTokenizer(ip, ".");
        campo1 = sepIp.nextToken();
        campo2 = sepIp.nextToken();
        campo3 = sepIp.nextToken();

        if (clase.equals("C")) {
            for (int i = 0; i < 256; i++) {

                if (!ip.equals(campo1 + "." + campo2 + "." + campo3 + "." + Integer.toString(i))) {
                    sR.add(campo1 + "." + campo2 + "." + campo3 + "." + Integer.toString(i));
                    System.out.println(campo1 + "." + campo2 + "." + campo3 + "." + Integer.toString(i));
                }
            }
        }
        if (clase.equals("B")) {
            for (int i = 0; i < 256; i++) {
                for (int j = 0; j < 256; j++) {
                    if (!ip.equals(campo1 + "." + campo2 + "." + Integer.toString(i) + "." + Integer.toString(j))) {
                        sR.add(campo1 + "." + campo2 + "." + Integer.toString(i) + "." + Integer.toString(j));
                        System.out.println(campo1 + "." + campo2 + "." + Integer.toString(i) + "." + Integer.toString(j));
                    }

                }
            }
        }
        if (clase.equals("A")) {
            for (int i = 0; i < 256; i++) {
                for (int j = 0; j < 256; j++) {
                    for (int k = 0; k < 256; k++) {
                        if (!ip.equals(campo1 + "." + Integer.toString(i) + "." + Integer.toString(j) + "." + Integer.toString(k))) {
                            sR.add(campo1 + "." + Integer.toString(i) + "." + Integer.toString(j) + "." + Integer.toString(k));
                            System.err.println(campo1 + "." + Integer.toString(i) + "." + Integer.toString(j) + "." + Integer.toString(k));
                        }

                    }
                }
            }
        }

    }
}
