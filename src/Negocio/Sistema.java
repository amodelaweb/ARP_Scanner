/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import UtilsARP.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SANTI
 */
public class Sistema implements Serializable {

    private List<Dispositivo> dispositivos;
    private List<Subred> subredes;
    private int selectedint;

    public Sistema(List<Subred> subredes) {
        this.dispositivos = new ArrayList<Dispositivo>();
        this.subredes = Utils.ObetenerDispositivosInterfaz();
    }

    public List<Dispositivo> getDispositivos() {
        return dispositivos;
    }

    public void setDispositivos(List<Dispositivo> dispositivos) {
        this.dispositivos = dispositivos;
    }

    public List<Subred> getSubredes() {
        return subredes;
    }

    public void setSubredes(List<Subred> subredes) {
        this.subredes = subredes;
    }

    public int getSelectedint() {
        return selectedint;
    }

    public void setSelectedint(int selectedint) {
        this.selectedint = selectedint;
    }

}
