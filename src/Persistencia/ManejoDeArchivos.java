/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Negocio.Dispositivo;
import Negocio.Sistema;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author SANTI
 */
public class ManejoDeArchivos {

    public static boolean guardarSistema(Sistema sistema , String pathArchivo, String nombreArchivo) throws Exception {
        File outFile = new File(pathArchivo + "/" + nombreArchivo);
        FileOutputStream outStream = null;
        ObjectOutputStream dataOutStream = null;
        boolean resultado = false;
        try {
            outStream = new FileOutputStream(outFile);
            dataOutStream = new ObjectOutputStream(outStream);
            dataOutStream.writeObject(sistema);
            resultado = true;
        } catch (FileNotFoundException e) {
            throw new IOException("Error en la ruta del archivo: " + e.getMessage());
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
            throw new IOException("Error grabando el archivo: " + e.getMessage());
        } catch (Exception e) {
            throw new Exception("Error al guardar el archivo: " + e.getMessage());
        } finally {
            try {
                dataOutStream.close();
                outStream.close();
            } catch (IOException e) {
                throw new IOException("excepcion cerrando el archivo:" + e.getMessage());
            }

        }
        return resultado;
    }

    public static Sistema cargarSistema(String nombreArch, String path) throws Exception {
        File inFile = new File(path + "/" + nombreArch);
        FileInputStream inStream = null;
        ObjectInputStream dataInStream = null;
        Sistema sistema = null;
        try {
            inStream = new FileInputStream(inFile);
            dataInStream = new ObjectInputStream(inStream);
            sistema = (Sistema) dataInStream.readObject();
            Dispositivo.setCons(sistema.getDispositivos().size());
            return sistema;
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Error en la ruta del archivo" + e.getMessage());
        } catch (IOException e) {
            throw new IOException("Error leyendo el archivo:" + e.getMessage());
        } catch (Exception e) {
            throw new Exception("Excepcion inesperada:" + e.getMessage());
        } finally {
            try {
                dataInStream.close();
                inStream.close();
            } catch (IOException e) {
                throw new IOException("excepcion cerrando el archivo:" + e.getMessage());
            }
        }
    }
}
